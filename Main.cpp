#include <iostream>
#include <regex>
#include <fstream>
using namespace std;


class Case
{
    public:
    /* data */
    Case(){}

    Case(char _type, int _Hauteur, int _ligne, int _colonne){
        Shortest_Distance_From_Start=std::numeric_limits<int>::max();
        type=_type;
        hauteur=_Hauteur;
        ligne=_ligne;
        colonne=_colonne;
        visitee=false;
        ArcDroit= Arc(_Hauteur,'O',_type);
        ArcBas= Arc(_Hauteur,'O',_type);
        ArcGauche= Arc(_Hauteur,'O',_type);
        ArcHaut= Arc(_Hauteur,'O',_type);
        ArcDiagonal_Bas_Droit=Arc(_Hauteur,'D',_type);
        ArcDiagonal_Bas_Gauche=Arc(_Hauteur,'D',_type);
        ArcDiagonal_Haut_Droit=Arc(_Hauteur,'D',_type);
        ArcDiagonal_Haut_Gauche=Arc(_Hauteur,'D',_type);

    }
       struct Arc
       {
           /* data */
            Arc(){}
            Arc(int Hauteur,char typeDep,char typeCase){
                PtrCase=nullptr;// si un deplacement est impossible il sera egale a nulptr
                HauteurDepart=Hauteur;
                typeDeplacement=typeDep;
                typeCaseDepart=typeCase;
                DeplacementImpossible=false;
                CoutDeplacement=std::numeric_limits<int>::max();//initialiser le cout deplacement a infini
                }
            Arc( const Arc & other ){
                HauteurDepart=other.HauteurDepart;
                typeCaseDepart=other.typeCaseDepart;
                CoutDeplacement=other.CoutDeplacement;
                typeDeplacement=other.typeDeplacement;
                DeplacementImpossible=other.DeplacementImpossible;
                };

            Case *PtrCase; //case darriver
            int HauteurDepart;
            char typeCaseDepart;
            double CoutDeplacement;
            char typeDeplacement;//orthogonal / diagonal
            bool DeplacementImpossible;// si ptrcase pointe vers une case impossible a atteindre

            void CalculCout(){
                if (PtrCase->type=='F')//case F
                {
                    /* code */
                    if(typeDeplacement=='O'){CoutDeplacement=2;}else
                    if(typeDeplacement=='D'){CoutDeplacement=2.8;}
                    
                }else if(typeCaseDepart=='R' && PtrCase->type=='R'){ //case R
                    if(typeDeplacement=='O'){CoutDeplacement=0.5;}else
                    if(typeDeplacement=='D'){CoutDeplacement=0.7;}
                }else if(PtrCase->type=='E'){DeplacementImpossible=true;}else
                {
                    if(typeDeplacement=='O'){CoutDeplacement=1.0;}else
                    if(typeDeplacement=='D'){CoutDeplacement=1.4;}
                    
                }
                //Prise en compte des hauteurs
                if(PtrCase->hauteur-HauteurDepart == 1){CoutDeplacement=CoutDeplacement*2;}else
                if(PtrCase->hauteur-HauteurDepart == 0 || PtrCase->hauteur-HauteurDepart == -1 ){/* cout reste inchangee*/}
                else if(PtrCase->hauteur-HauteurDepart>=2 || PtrCase->hauteur-HauteurDepart<=-2){DeplacementImpossible=true;}
                
                
            }
        };
    //constructeur de copie
    Case( const Case & other )
     {
         ligne=other.ligne;
         colonne=other.colonne;
         type=other.type;
         hauteur=other.hauteur;
         visitee=other.visitee;
         Shortest_Distance_From_Start=other.Shortest_Distance_From_Start;
         ArcHaut=other.ArcHaut;
         ArcBas=other.ArcBas;
         ArcGauche=other.ArcGauche;
         ArcDroit=other.ArcDroit;
         ArcDiagonal_Bas_Droit=other.ArcDiagonal_Bas_Droit;
         ArcDiagonal_Bas_Gauche=other.ArcDiagonal_Bas_Gauche;
         ArcDiagonal_Haut_Droit=other.ArcDiagonal_Haut_Droit;
         ArcDiagonal_Haut_Gauche=other.ArcDiagonal_Haut_Gauche;
      
     }; 
   
   
   //position
    int ligne,colonne;
    //type & hauteur terrain
    char type; int hauteur;
    bool visitee;
    double Shortest_Distance_From_Start;
    Case *Previous_Vertex;
    //Arcs
    Arc ArcHaut;
    Arc ArcBas;
    Arc ArcGauche;
    Arc ArcDroit;
    Arc ArcDiagonal_Bas_Droit;
    Arc ArcDiagonal_Bas_Gauche;
    Arc ArcDiagonal_Haut_Droit;
    Arc ArcDiagonal_Haut_Gauche;
};


struct Univers
{
    /* data */
    Univers(){
        Lecture(colonnes,ligne,Matrix);
        LinkGraph();
        SetCostOnMatrix();
        PorteUnivers();
    }

    int ligne; int colonnes;
    int Optimal;// ordre optimal du plus court chemin.
    double Total;
    std::vector<std::vector<Case>> Matrix;
    Case *CaseCourante;
    Case *Porte; Case *Tresor1; Case *Tresor2; Case *Tresor3; 

    std::vector<std::vector<Case>> Create2DArray(int ligne, int colonne){
     vector<vector<Case>> Matrix;
     Matrix.resize(ligne+1);

     return Matrix;
    }   

    void Lecture ( int &colonnes ,int &ligne, std::vector<std::vector<Case>> &Matrix ){
     string nomFichier;
     cin>>nomFichier;
     string Dimensions;
     string Terrain;
     ifstream reader (nomFichier);

     //Lecture des dimesions de la matrice
     getline(reader,Dimensions);
     colonnes = atoi (  (Dimensions.substr(0,1).c_str()));
     ligne = atoi (  (Dimensions.substr(1,2).c_str()));
     Matrix= Create2DArray(colonnes,ligne);
     cout<<"Colonnes : "<<colonnes<<endl;
     cout<<"Lignes : "<< ligne<<endl;
   
     //Remplissage de la matrice
     for (int i = 0; i < ligne; i++)
     {
        int indexTerrain=0;
        getline(reader,Terrain);// prend la ligne i   
        //cout<<Terrain<<endl;    

        for (int j = 0; j < colonnes; j++) //pour chaque colonne de la ligne i
        {
            Case Caze = Case(*Terrain.substr(indexTerrain,1).c_str() ,atoi(Terrain.substr(indexTerrain+1,2).c_str()),i,j);
            Matrix[i].push_back(Caze);
            cout<<Matrix[i][j].type<<Matrix[i][j].hauteur<<" ";
            indexTerrain=indexTerrain+2;
        }
        cout<<endl;
     }  

     //Lecture des Coordonne des tresors
     cout<<"Lecture des Coordonne des tresors..."<<endl;
     string Tresor;
     getline(reader,Tresor);
     Tresor1 = &Matrix[atoi ((Tresor.substr(1,2).c_str()))][atoi ((Tresor.substr(0,1).c_str()))];
     getline(reader,Tresor);
     Tresor2 = &Matrix[atoi ((Tresor.substr(1,2).c_str()))][atoi ((Tresor.substr(0,1).c_str()))];
     getline(reader,Tresor);
     Tresor3 = &Matrix[atoi ((Tresor.substr(1,2).c_str()))][atoi ((Tresor.substr(0,1).c_str()))];
     
    }

    void LinkGraph(){
        cout<<"Proceed to Graph linking..."<<endl;
        //horizontal link
        for (int i = 0; i < ligne; i++)
        {
            /* code */
            for (int j = 0; j < colonnes; j++)
            {
                //si cest la premiere colonne
                if(j==0){
                    Matrix[i][j].ArcGauche.PtrCase= &Matrix[i][colonnes-1]; Matrix[i][j].ArcDroit.PtrCase= &Matrix[i][j+1];}else//pointe vers la dernier col
                //si c'est la derniere colonne
                if(j==colonnes-1){Matrix[i][j].ArcDroit.PtrCase= &Matrix[i][0]; Matrix[i][j].ArcGauche.PtrCase= &Matrix[i][j-1];}else // on reviens a la col 0
                {
                    //on lie chaque case avec son suivant et son precedant
                    Matrix[i][j].ArcDroit.PtrCase= &Matrix[i][j+1];
                    Matrix[i][j].ArcGauche.PtrCase= &Matrix[i][j-1];
                    
                }

               //cout<<Matrix[i][j].ArcGauche.PtrCase->type<<Matrix[i][j].ArcGauche.PtrCase->hauteur;
                
            }
            //cout<<endl;
        }
        
        //vertical link
        for (int i = 0; i < ligne; i++)
        {
            for (int j = 0; j < colonnes; j++)
            {
                //si cest la premiere ligne
                if(i==0){Matrix[i][j].ArcBas.PtrCase= &Matrix[i+1][j];}else
                //si cest la derniere ligne
                if(i==ligne-1){Matrix[i][j].ArcHaut.PtrCase= &Matrix[i-1][j];}else
                {
                    Matrix[i][j].ArcBas.PtrCase= &Matrix[i+1][j];
                    Matrix[i][j].ArcHaut.PtrCase= &Matrix[i-1][j];
                }
               //cout<<Matrix[i][j].ArcHaut.PtrCase->type<<Matrix[i][j].ArcHaut.PtrCase->hauteur;
            }
           //cout<<endl;
            
        }

        //diagonal link
        for (int i = 0; i < ligne; i++)
        {
            /* code */
            for (int j = 0; j < colonnes; j++)
            {
                /* code */
                //si ce nest pas la derniere ligne
                if(i!=ligne-1){
                    Matrix[i][j].ArcDiagonal_Bas_Droit.PtrCase= Matrix[i][j].ArcDroit.PtrCase->ArcBas.PtrCase;
                    Matrix[i][j].ArcDiagonal_Bas_Gauche.PtrCase= Matrix[i][j].ArcGauche.PtrCase->ArcBas.PtrCase;
                    }
                //si ce nest pas la premiere ligne
                if (i!=0)
                {
                    Matrix[i][j].ArcDiagonal_Haut_Droit.PtrCase= Matrix[i][j].ArcDroit.PtrCase->ArcHaut.PtrCase;
                    Matrix[i][j].ArcDiagonal_Haut_Gauche.PtrCase= Matrix[i][j].ArcGauche.PtrCase->ArcHaut.PtrCase;
                }
                
            }
            
        }
        
         //Matrix[2][2].ArcDiagonal_Bas_Droit.CalculCout();
         //cout<< "case cost : " << Matrix[2][2].ArcDiagonal_Bas_Droit.CoutDeplacement<<endl;
        


    }

    void SetCostOnMatrix(){
        for (int i = 0; i < ligne; i++)
        {
            for (int j = 0; j < colonnes; j++)
            {
                //calculs des cout horizontaux (gauche droites)
                Matrix[i][j].ArcDroit.CalculCout();
                Matrix[i][j].ArcGauche.CalculCout();

                //calculs des couts verticaux(bas Haut)
                if(i!=0){Matrix[i][j].ArcHaut.CalculCout();}
                if(i!=ligne-1){Matrix[i][j].ArcBas.CalculCout();}

                //calculs des cout Diagonaux
                //vers le haut
                if(i!=0){Matrix[i][j].ArcDiagonal_Haut_Droit.CalculCout();}
                if(i!=0){Matrix[i][j].ArcDiagonal_Haut_Gauche.CalculCout();}
                //vers le bas
                if(i!=ligne-1){Matrix[i][j].ArcDiagonal_Bas_Droit.CalculCout();}
                if(i!=ligne-1){Matrix[i][j].ArcDiagonal_Bas_Gauche.CalculCout();}
                
            }
            
        }
    
    }

    void PorteUnivers(){
        //position porte
        for (int i = 0; i < ligne; i++)
        {
            for (int j = 0; j < colonnes; j++)
            {
                if (Matrix[i][j].type=='P')
                {
                    /* code */
                    Porte=&Matrix[i][j];//porte trouvee
                }
                
            }
            
        }
        cout<<"Porte : "<< Porte->type<<Porte->hauteur<<endl;
        cout<<"position porte : "<<Porte->ligne <<","<< Porte->colonne<<endl;
        

    }

    void ClearMatrix(){

        for (int i = 0; i < ligne; i++)
        {
            for (int j = 0; j < colonnes; j++)
            {
              //clear All Shortest Distances
              Matrix[i][j].Shortest_Distance_From_Start=std::numeric_limits<int>::max();

             //Clear All Visitee as false
             Matrix[i][j].visitee=false;
            }
            
        }
    }

    void dijkstraAlgorithm (Case &StartVertex){
        //cout<<"Running DijkstraAlgorithm on the matrix..."<<endl;
        int Run=0;

        //Let distance from start vertex = 0
        StartVertex.Shortest_Distance_From_Start=0; //All other verticles = infinity
        Case *CurrentVertex=&StartVertex;
       

        //Do
        do{
         //visit the unvisited vertex with the smallest known distance from the start vertex
         Case *VertexWithTheSmalestKnownD = new Case('x', 0, 0, 0);//refresh the VertexWithTheSmalestKnownD
         

         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].visitee==false){VertexWithTheSmalestKnownD=&Matrix[CurrentVertex->ligne][CurrentVertex->colonne];if(Run==2)cout<<"entree1"<<endl;}
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDroit.DeplacementImpossible==false&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDroit.PtrCase->visitee==false){
             if( Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDroit.PtrCase->Shortest_Distance_From_Start<VertexWithTheSmalestKnownD->Shortest_Distance_From_Start){
                 VertexWithTheSmalestKnownD=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDroit.PtrCase;
               }
            }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcGauche.DeplacementImpossible==false&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcGauche.PtrCase->visitee==false){
             if( Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcGauche.PtrCase->Shortest_Distance_From_Start<VertexWithTheSmalestKnownD->Shortest_Distance_From_Start){
                 VertexWithTheSmalestKnownD=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcGauche.PtrCase;
               }
         }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcHaut.PtrCase!=nullptr &&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcHaut.DeplacementImpossible==false &&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcHaut.PtrCase->visitee==false){
             if( Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcHaut.PtrCase->Shortest_Distance_From_Start<VertexWithTheSmalestKnownD->Shortest_Distance_From_Start){
                 VertexWithTheSmalestKnownD=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcHaut.PtrCase;
               }
         }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcBas.PtrCase!=nullptr&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcBas.DeplacementImpossible==false&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcBas.PtrCase->visitee==false){
             if( Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcBas.PtrCase->Shortest_Distance_From_Start<VertexWithTheSmalestKnownD->Shortest_Distance_From_Start){
                 VertexWithTheSmalestKnownD=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcBas.PtrCase;
               }
         }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Droit.PtrCase!=nullptr &&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Droit.DeplacementImpossible==false&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Droit.PtrCase->visitee==false){
             if( Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Droit.PtrCase->Shortest_Distance_From_Start<VertexWithTheSmalestKnownD->Shortest_Distance_From_Start){
                 VertexWithTheSmalestKnownD=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Droit.PtrCase;
               }
         }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Gauche.PtrCase!=nullptr&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Gauche.DeplacementImpossible==false&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Gauche.PtrCase->visitee==false){
              if( Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Gauche.PtrCase->Shortest_Distance_From_Start<VertexWithTheSmalestKnownD->Shortest_Distance_From_Start){
                 VertexWithTheSmalestKnownD=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Gauche.PtrCase;
                }
         }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Droit.PtrCase!=nullptr&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Droit.DeplacementImpossible==false&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Droit.PtrCase->visitee==false){
             if( Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Droit.PtrCase->Shortest_Distance_From_Start<VertexWithTheSmalestKnownD->Shortest_Distance_From_Start){
                 VertexWithTheSmalestKnownD=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Droit.PtrCase;
               }
         }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Gauche.PtrCase!=nullptr&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Gauche.DeplacementImpossible==false&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Gauche.PtrCase->visitee==false){
             if( Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Gauche.PtrCase->Shortest_Distance_From_Start<VertexWithTheSmalestKnownD->Shortest_Distance_From_Start){
                 VertexWithTheSmalestKnownD=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Gauche.PtrCase;
               }
         }

        if(Matrix[VertexWithTheSmalestKnownD->ligne][VertexWithTheSmalestKnownD->colonne].visitee==false){
         
         Matrix[VertexWithTheSmalestKnownD->ligne][VertexWithTheSmalestKnownD->colonne].Previous_Vertex=CurrentVertex;
         CurrentVertex= & Matrix[VertexWithTheSmalestKnownD->ligne][VertexWithTheSmalestKnownD->colonne];
           
        }
  
         //cout<<"//-----"<<endl;
         //cout<<"CurrentVertex : "<<Matrix[CurrentVertex->ligne][CurrentVertex->colonne].type<<endl;
         //cout<<"position : "<<Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ligne << Matrix[CurrentVertex->ligne][CurrentVertex->colonne].colonne<<endl;
         //cout<<"Smallest Distance : "<<Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start<<endl; 
         //cout<<"visited : "<< Matrix[CurrentVertex->ligne][CurrentVertex->colonne].visitee<<endl;
         if( Matrix[CurrentVertex->ligne][CurrentVertex->colonne].visitee==true){CurrentVertex=&Matrix[CurrentVertex->Previous_Vertex->ligne][CurrentVertex->Previous_Vertex->colonne];}


         //For the current vertex, examine its unvisited neighbours
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDroit.DeplacementImpossible==false){
             if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDroit.CoutDeplacement<Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDroit.PtrCase->Shortest_Distance_From_Start){
                 Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDroit.PtrCase->Shortest_Distance_From_Start=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDroit.CoutDeplacement;
                }
             }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcGauche.DeplacementImpossible==false){
              if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcGauche.CoutDeplacement<Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcGauche.PtrCase->Shortest_Distance_From_Start){
                 Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcGauche.PtrCase->Shortest_Distance_From_Start=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcGauche.CoutDeplacement;
                }
         }
         
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcHaut.PtrCase!=nullptr &&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcHaut.DeplacementImpossible==false ){
             if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcHaut.CoutDeplacement<Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcHaut.PtrCase->Shortest_Distance_From_Start){
                 Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcHaut.PtrCase->Shortest_Distance_From_Start=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcHaut.CoutDeplacement;
                }
         }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcBas.PtrCase!=nullptr&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcBas.DeplacementImpossible==false){
             if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcBas.CoutDeplacement<Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcBas.PtrCase->Shortest_Distance_From_Start){
                 Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcBas.PtrCase->Shortest_Distance_From_Start=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcBas.CoutDeplacement;
                }
         }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Droit.PtrCase!=nullptr &&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Droit.DeplacementImpossible==false){
             if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Droit.CoutDeplacement<Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Droit.PtrCase->Shortest_Distance_From_Start){
                 Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Droit.PtrCase->Shortest_Distance_From_Start=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Droit.CoutDeplacement;
                }
         }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Gauche.PtrCase!=nullptr&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Gauche.DeplacementImpossible==false){
             if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Gauche.CoutDeplacement<Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Gauche.PtrCase->Shortest_Distance_From_Start){
                 Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Gauche.PtrCase->Shortest_Distance_From_Start=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Bas_Gauche.CoutDeplacement;
                }
         }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Droit.PtrCase!=nullptr&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Droit.DeplacementImpossible==false){
             if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Droit.CoutDeplacement<Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Droit.PtrCase->Shortest_Distance_From_Start){
                 Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Droit.PtrCase->Shortest_Distance_From_Start=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Droit.CoutDeplacement;
                }
         }
         if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Gauche.PtrCase!=nullptr&&Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Gauche.DeplacementImpossible==false){
             if(Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Gauche.CoutDeplacement<Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Gauche.PtrCase->Shortest_Distance_From_Start){
                 Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Gauche.PtrCase->Shortest_Distance_From_Start=Matrix[CurrentVertex->ligne][CurrentVertex->colonne].Shortest_Distance_From_Start+Matrix[CurrentVertex->ligne][CurrentVertex->colonne].ArcDiagonal_Haut_Gauche.CoutDeplacement;
                }
         }

         

         // make the current vertex as visited]
         Matrix[CurrentVertex->ligne][CurrentVertex->colonne].visitee=true;
         Run++;

        }while(Run<=(ligne*colonnes*3));
        //Unil All verticles are visited
    }
   
    void OptimisationDesDistances(){
        cout<<"lancement de l'optimisation des distances..."<<endl<<"Utilisation de l'algorithm de dijikstra..."<<endl;
        vector<Case> OrdreCase;
        Case *Depart, *Arrive;
        double Distance=0;
        double DistanceFinal=std::numeric_limits<int>::max();
        //Ordre 123
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
                

            }
            ClearMatrix();
            
        }
        
        if(Distance<DistanceFinal){DistanceFinal=Distance;Optimal=1;}
        //Ordre 132
        OrdreCase.clear();
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
                

            }
            ClearMatrix();
            
        }
        if(Distance<DistanceFinal){DistanceFinal=Distance;Optimal=2;}
        //Ordre 213
        OrdreCase.clear();
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
                

            }
            ClearMatrix();
            
        }
        if(Distance<DistanceFinal){DistanceFinal=Distance;Optimal=3;}
        //Ordre 231
        OrdreCase.clear();
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
                

            }
            ClearMatrix();
            
        }
        if(Distance<DistanceFinal){DistanceFinal=Distance;Optimal=4;}
        //Ordre 312
        OrdreCase.clear();
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
                

            }
            ClearMatrix();
            
        }
        if(Distance<DistanceFinal){DistanceFinal=Distance;Optimal=5;}
        //Ordre 321
        OrdreCase.clear();
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
                

            }
            ClearMatrix();
            
        }
        if(Distance<DistanceFinal){DistanceFinal=Distance;Optimal=6;}
        Total=DistanceFinal;


    }

    void Affichage(){
        cout<<"Resultat: "<<endl;
        vector<Case> OrdreCase;
        Case *Depart, *Arrive;
        double Distance;
        double DistanceFinal=0;
      if(Optimal==1){
        //Ordre 123
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        int TresorIndex=0;
       
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
               
                
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
   
                 if(i==1){cout<<"Porte"<< " -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<< endl;}
                 else if(i==2||i==3){cout<<"T"<<TresorIndex<<" -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<<endl;}
                 else if(i==4){cout<<"T"<<i-1<<" -> " <<"Porte : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start <<endl;}
                 TresorIndex++;

            
            }
            ClearMatrix();
            
        }
        cout<<"Total : "<<Total<<endl;;
     
        }else if(Optimal==2){
        //Ordre 132
        OrdreCase.clear();
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        int TresorIndex=0;
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
                if(i==1){cout<<"Porte"<< " -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<< endl;}
                 else if(i==2||i==3){cout<<"T"<<TresorIndex<<" -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<<endl;}
                 else if(i==4){cout<<"T"<<i-1<<" -> " <<"Porte : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start <<endl;}
                 TresorIndex++;


            }
            ClearMatrix();
            
        }
        cout<<"Total : "<<Total<<endl;;
        }else if(Optimal==3){
        //Ordre 213
        OrdreCase.clear();
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        int TresorIndex=0;
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
                if(i==1){cout<<"Porte"<< " -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<< endl;}
                 else if(i==2||i==3){cout<<"T"<<TresorIndex<<" -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<<endl;}
                 else if(i==4){cout<<"T"<<i-1<<" -> " <<"Porte : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start <<endl;}
                 TresorIndex++;


            }
            ClearMatrix();
            
        }
        cout<<"Total : "<<Total<<endl;;
  
        }else if(Optimal==4){
        //Ordre 231
        OrdreCase.clear();
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        int TresorIndex=0;
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
                if(i==1){cout<<"Porte"<< " -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<< endl;}
                 else if(i==2||i==3){cout<<"T"<<TresorIndex<<" -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<<endl;}
                 else if(i==4){cout<<"T"<<i-1<<" -> " <<"Porte : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start <<endl;}
                 TresorIndex++;


            }
            ClearMatrix();
            
        }
        cout<<"Total : "<<Total<<endl;;
 
        }else if(Optimal==5){
        //Ordre 312
        OrdreCase.clear();
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        int TresorIndex=0;
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
                if(i==1){cout<<"Porte"<< " -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<< endl;}
                 else if(i==2||i==3){cout<<"T"<<TresorIndex<<" -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<<endl;}
                 else if(i==4){cout<<"T"<<i-1<<" -> " <<"Porte : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start <<endl;}
                 TresorIndex++;


            }
            ClearMatrix();
            
        }
        cout<<"Total : "<<Total<<endl;;
        }else if (Optimal==6){
        //Ordre 321
        OrdreCase.clear();
        OrdreCase.push_back(*Porte);
        OrdreCase.push_back(*Tresor3);
        OrdreCase.push_back(*Tresor2);
        OrdreCase.push_back(*Tresor1);
        OrdreCase.push_back(*Porte);
        Depart= Porte;
        int TresorIndex=0;
        //du depart au tresors suivant
        Distance=0;
        for (int i = 1; i < OrdreCase.size(); i++)
        {
            dijkstraAlgorithm(*Depart);

            if (Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<9999)
            {
                /* code */
                Distance=Distance+Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start;
                Depart=&Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne];
                if(i==1){cout<<"Porte"<< " -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<< endl;}
                 else if(i==2||i==3){cout<<"T"<<TresorIndex<<" -> " <<"T" << i << " : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start<<endl;}
                 else if(i==4){cout<<"T"<<i-1<<" -> " <<"Porte : "<<Matrix[OrdreCase[i].ligne][OrdreCase[i].colonne].Shortest_Distance_From_Start <<endl;}
                 TresorIndex++;
            }
            ClearMatrix();
            
        }
        cout<<"Total : "<<Total<<endl;;
        }


    }
        
 
};



int main(int argc, char const *argv[])
{
    //Creation de lunivers
    Univers univers= Univers();
    univers.OptimisationDesDistances();
    univers.Affichage();


    return 0;
}
// Date limite : 28 juillet
//g++ nomEtudiantTP1.cpp -lm -std=c++11